package com.kurtlike.bluetoothcar.gamepad;

import android.view.InputDevice;
import android.view.InputEvent;
import android.view.KeyEvent;
import android.view.MotionEvent;

public class Dpad {
    final static int UP       = KeyEvent.KEYCODE_DPAD_UP;
    final static int LEFT     = KeyEvent.KEYCODE_DPAD_LEFT;
    final static int RIGHT    = KeyEvent.KEYCODE_DPAD_RIGHT;
    final static int DOWN     = KeyEvent.KEYCODE_DPAD_DOWN;
    final static int CENTER   = KeyEvent.KEYCODE_DPAD_CENTER;
    int directionPressed = -1;

    public int getDirectionPressed(InputEvent event) {
        directionPressed = -1;
        if (!isDpadDevice(event)) {
            return -1;
        }


        if (event instanceof MotionEvent) {

            MotionEvent motionEvent = (MotionEvent) event;
            float xaxis = motionEvent.getAxisValue(MotionEvent.AXIS_HAT_X);
            float yaxis = motionEvent.getAxisValue(MotionEvent.AXIS_HAT_Y);

            if (Float.compare(xaxis, -1.0f) == 0) {
                directionPressed =  Dpad.LEFT;
            } else if (Float.compare(xaxis, 1.0f) == 0) {
                directionPressed =  Dpad.RIGHT;
            }
            else if (Float.compare(yaxis, -1.0f) == 0) {
                directionPressed =  Dpad.UP;
            } else if (Float.compare(yaxis, 1.0f) == 0) {
                directionPressed =  Dpad.DOWN;
            }
        }

        else if (event instanceof KeyEvent) {

            KeyEvent keyEvent = (KeyEvent) event;
            switch (keyEvent.getKeyCode()){
                case KeyEvent.KEYCODE_DPAD_LEFT:{
                    directionPressed = Dpad.LEFT;
                    break;
                }
                case KeyEvent.KEYCODE_DPAD_RIGHT:{
                    directionPressed = Dpad.RIGHT;
                    break;
                }
                case KeyEvent.KEYCODE_DPAD_UP:{
                    directionPressed = Dpad.UP;
                    break;
                }
                case KeyEvent.KEYCODE_DPAD_DOWN:{
                    directionPressed = Dpad.DOWN;
                    break;
                }
                case KeyEvent.KEYCODE_DPAD_CENTER:{
                    directionPressed = Dpad.CENTER;
                    break;
                }
            }
        }
        return directionPressed;
    }

    public static boolean isDpadDevice(InputEvent event) {
        return (event.getSource() & InputDevice.SOURCE_DPAD)
                != InputDevice.SOURCE_DPAD;
    }
}