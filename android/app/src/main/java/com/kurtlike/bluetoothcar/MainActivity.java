package com.kurtlike.bluetoothcar;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.bluetooth.*;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.TextView;

import com.kurtlike.bluetoothcar.gamepad.Gamepad;
import com.kurtlike.bluetoothcar.gamepad.GamepadStickHandleValue;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import Generator.Generator;


public class MainActivity extends AppCompatActivity {
    private BluetoothAdapter mBTAdapter;
    private Set<BluetoothDevice> mPairedDevices;
    private final int GEARUP = 103;
    private final int GEARDOWN = 102;
    private final int STARTENGINE = 100;

    private final int MAXRPM = 12700;
    private final double WHEELM = 0.457;
    private final static int REQUEST_ENABLE_BT = 1;
    private TextView gearView;
    private TextView tachometerView;
    private TextView speedoView;
    private static final UUID BT_MODULE_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private OutputStream mmOutStream;
    boolean block = false;
    private final Gamepad gamepad = new Gamepad();
    int wheel_v = 0;
    int gas_v = 0;
    private final Generator generator = new Generator();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gearView = (TextView)findViewById(R.id.gearView);
        tachometerView = (TextView)findViewById(R.id.tachometer);
        speedoView = (TextView)findViewById(R.id.speedo);
        generator.setSimParams(100, 100);
        bluetoothInit();
        Timer myTimer = new Timer();
        myTimer.schedule(new TimerTask() {
            public void run() {
                blSend();
                viewUpdate();
            }
        }, 20, 80);

    }

        public void blSend() {
        int trRpm = generator.getTransmissionOutRpm();
        gas_v = 100 * trRpm/MAXRPM;
        String send = wheel_v  + " "+ gas_v + "\n";
        byte[] bytes = send.getBytes();
        try {
            mmOutStream.write(bytes);
        } catch (IOException ignored) { }
    }
    @Override
    public boolean onGenericMotionEvent(MotionEvent event) {
        GamepadStickHandleValue value = gamepad.handleSticks(event);
        if(value.button < 0) return true;

        int accelerate = value.rightTrigger - value.leftTrigger;
        wheel_v = value.leftX;
        generator.gasUpdate(((float)accelerate)/100);
        return true;
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode){
            case STARTENGINE:{
                generator.engineStart();
                break;
            }
            case GEARUP:{
                generator.gearUpdate(generator.getGear()+1);
                gearView.setText(generator.getGear().toString());
                break;
            }
            case GEARDOWN:{
                generator.gearUpdate(generator.getGear()-1);
                gearView.setText(generator.getGear().toString());
                break;
            }
        }
        return true;
    }

    @SuppressLint({"MissingPermission", "HardwareIds"})
    private void bluetoothInit(){
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            System.out.println("bruh");
        }
        BluetoothDevice hc = null;
        for(BluetoothDevice bt:bluetoothAdapter.getBondedDevices()){
            if(bt.getName().equals("HC-05")){
                hc = bt;
                break;
            }

        }
        BluetoothSocket mmSocket;
        Method m;
        try {
            m = hc.getClass().getMethod("createInsecureRfcommSocketToServiceRecord", UUID.class);
            mmSocket = (BluetoothSocket) m.invoke(hc, BT_MODULE_UUID);
            mmSocket.connect();
            mmOutStream = mmSocket.getOutputStream();
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException |
                 IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void viewUpdate(){
        this.runOnUiThread(viewUpdateRunner);
    }
    private Runnable viewUpdateRunner = new Runnable() {
        public void run() {
            int trRpm = generator.getTransmissionOutRpm();
            int speed = speedCalculate(trRpm);
            tachometerView.setText(String.valueOf(generator.getEngineRpm().intValue()));
            speedoView.setText(String.valueOf(speed));

        }
    };
    private int speedCalculate(int rpm){
        return (int)((rpm/60) * Math.PI * WHEELM * 3.6 * 0.2);
    }
}
