package com.kurtlike.bluetoothcar.gamepad;

import android.view.InputDevice;
import android.view.MotionEvent;

public class MotionSticks {

  private  GamepadStickHandleValue value;


    public GamepadStickHandleValue getMotionSticks(MotionEvent event){
        value = new GamepadStickHandleValue();
        if ((event.getSource() & InputDevice.SOURCE_JOYSTICK) ==
                InputDevice.SOURCE_JOYSTICK &&
                event.getAction() == MotionEvent.ACTION_MOVE) {
            final int historySize = event.getHistorySize();
            for (int i = 0; i < historySize; i++) {

                processMotionSticksInput(event, i);
            }
            processMotionSticksInput(event, -1);
        }
        return value;
    }
    public void processMotionSticksInput(MotionEvent event, int historyPos) {
        InputDevice inputDevice = event.getDevice();
        value.button = 0;
        value.leftX = (int)(100*getCenteredAxis(event, inputDevice,
                MotionEvent.AXIS_X, historyPos));
        value.rightX = (int)(100*getCenteredAxis(event, inputDevice,
                    MotionEvent.AXIS_Z, historyPos));
        value.leftY = (int)(-100*getCenteredAxis(event, inputDevice,
                MotionEvent.AXIS_Y, historyPos));
        value.rightY = (int)(-100*getCenteredAxis(event, inputDevice,
                    MotionEvent.AXIS_RZ, historyPos));
        value.rightTrigger = (int)(100*getCenteredAxis(event, inputDevice,
                    MotionEvent.AXIS_GAS, historyPos));
        value.leftTrigger = (int)(100*getCenteredAxis(event, inputDevice,
                    MotionEvent.AXIS_BRAKE, historyPos));

    }
    private  float getCenteredAxis(MotionEvent event,
                                         InputDevice device, int axis, int historyPos) {
        final InputDevice.MotionRange range =
                device.getMotionRange(axis, event.getSource());

        if (range != null) {
            final float flat = range.getFlat();
            final float value =
                    historyPos < 0 ? event.getAxisValue(axis):
                            event.getHistoricalAxisValue(axis, historyPos);
            if (Math.abs(value) > flat) {
                return value;
            }
        }
        return 0;
    }

}
