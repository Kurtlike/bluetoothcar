package com.kurtlike.bluetoothcar.gamepad;

import android.view.MotionEvent;

public class Gamepad {
    private Dpad dpad;
    private MotionSticks motionSticks;

    public Gamepad() {
        dpad = new Dpad();
             motionSticks = new MotionSticks();
    }
    public GamepadStickHandleValue handleSticks(MotionEvent event){
        GamepadStickHandleValue retVal = new GamepadStickHandleValue();
        if( (retVal.button = dpad.getDirectionPressed(event)) >= 0){
           return retVal;
        }
        return motionSticks.getMotionSticks(event);
    }
}
