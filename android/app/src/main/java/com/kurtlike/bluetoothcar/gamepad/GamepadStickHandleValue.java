package com.kurtlike.bluetoothcar.gamepad;

public class GamepadStickHandleValue {
    public int button = -1;
    public int leftX = 0;
    public int leftY = 0;
    public int rightX = 0;
    public int rightY = 0;
    public int leftTrigger = 0;
    public int rightTrigger = 0;
}
