(ns Generator.Generator
  (:gen-class
    :main false
    :prefix "-"
    :methods [[setSimParams [java.lang.Integer java.lang.Integer] void]
              [gasUpdate [java.lang.Float] void]
              [breakUpdate [java.lang.Float] void]
              [gearUpdate [java.lang.Integer] void]
              [getEngineRpm [] java.lang.Integer]
              [getTransmissionOutRpm [] java.lang.Integer]
              [getGear [] java.lang.Integer]
              [engineStart [] void]])
  (:require
    [clojure.core.async
     :as a
     :refer [>!! <!! chan]]))


(def gears-number 6)
(def gear-ratios [0 2.66 1.78 1.3 1.0 0.8 0.63])
(def max-engine-speed 8000)
(def max-power-engine-speed 4800)
(def idling 1000)
(def engine-rpm (atom 0))
(def engine-start (atom false))
(def transmission-out-rpm (atom 0))
(def update-time (atom 10))
(def current-gas (atom 0.0))
(def current-break (atom 0.0))
(def current-gear (atom 0))
(def gas-increase-speed (atom 100))
(def engine-out-rpm (chan))
(def rpm-correct 200)
(def reeling-down 100)
(def idling-down 150)
(def gas-threshold 0.1)


(defn gas-efficiency
  [gas curr-rpm]
  (let [rpm curr-rpm]
    (if (< rpm max-power-engine-speed)
      (* gas (+ 0.4 (* 0.6 (/ rpm max-power-engine-speed))))
      (* gas (- 1 (/ (- rpm max-power-engine-speed) (- max-engine-speed max-power-engine-speed)))))))


(defn rpm-calculate
  [curr-rpm gas]
  (let [res-gas (gas-efficiency  gas curr-rpm)]
    (if (< res-gas gas-threshold)
      (if (> (- curr-rpm idling) reeling-down)
        (- curr-rpm idling-down)
        idling)
      (+ curr-rpm (* @gas-increase-speed res-gas)))))


(def engine
  (future
    (while true
      (do
        (Thread/sleep @update-time)
        (if @engine-start
          (let [rpm @engine-rpm gas @current-gas]
            (swap! engine-rpm rpm-calculate gas)
            (>!! engine-out-rpm rpm))
          nil)))))


(defn transmission-ratios-calculate
  [input-rpm]
  (let [gear @current-gear]
    (cond
      (> gear 0) (/ input-rpm (get gear-ratios gear))
      (< gear 0) (- 0 input-rpm)
      :else 0)))


(def transmission
  (future
    (while true
      (let [input-rpm (<!! engine-out-rpm)]
        (if (not= @current-gear 0)
          (reset! transmission-out-rpm (transmission-ratios-calculate input-rpm))
          nil)))))


(defn gear-down
  [new-gear]
  (let [priv-gear  @current-gear]
    (reset! current-gear new-gear)
    (let [new-motor-speed (#(* (/ @transmission-out-rpm (get gear-ratios priv-gear)) (get gear-ratios @current-gear)))]
      (if (> new-motor-speed max-engine-speed)
        (reset! engine-rpm max-engine-speed)
        (reset! engine-rpm (+ new-motor-speed rpm-correct))))))


(defn gear-up
  [new-gear]
  (let [priv-gear  @current-gear]
    (reset! current-gear new-gear)
    (if (= 0 priv-gear)
      (reset! engine-rpm (- @engine-rpm rpm-correct))
      (let [new-motor-speed (#(* (/ (max @transmission-out-rpm idling) (get gear-ratios priv-gear)) (get gear-ratios @current-gear)))]
        (if (> (- new-motor-speed rpm-correct) idling)
          (reset! engine-rpm (- new-motor-speed rpm-correct))
          nil)))))


(def gear-neitral
  (future
    (while true
      (do
        (Thread/sleep @update-time)
        (if (= @current-gear 0)
          (swap! transmission-out-rpm #(if (> %  reeling-down)
                                         (- % reeling-down)
                                         0))
          nil)))))


(defn -engineStart
  [_]
  (if @engine-start
    (do
      (reset! engine-start false)
      (reset! engine-rpm 0))
    (do
      (reset! engine-start true)
      (reset! engine-rpm idling))))


(defn -gasUpdate
  [_ gas]
  (reset! current-gas gas))


(defn -breakUpdate
  [_ break]
  (reset! current-break break))


(defn -gearUpdate
  [_ gear]
  (let [curr-gear @current-gear]
    (cond
      (or (< gear -1) (> gear gears-number)) (println "wrong gear")
      (= gear -1) (println "revirse gear")
      (= gear 0) (reset! current-gear 0)
      (> gear curr-gear) (gear-up gear)
      (< gear curr-gear) (gear-down gear))))


(defn -getGear
  [_]
  (int @current-gear))


(defn -getEngineRpm
  [_]
  (int @engine-rpm))


(defn -getTransmissionOutRpm
  [_]
  (int @transmission-out-rpm))


(defn -setSimParams
  [_ up-time gas-inc-speed]
  (reset! gas-increase-speed gas-inc-speed)
  (reset! update-time up-time))

