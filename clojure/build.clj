(ns build
  (:refer-clojure :exclude [test])
  (:require
    [clojure.tools.build.api :as b]))


(def build-folder "target")
(def jar-content (str build-folder "/classes"))
(def lib-name 'com.github.kurtlike/generator)
(def version "0.2.1")
(def basis (b/create-basis {:project "deps.edn"}))
(def jar-file-name (format "%s/%s-%s.jar" build-folder (name lib-name) version))


(defn clean
  [_]
  (b/delete {:path build-folder})
  (println (format "Build folder \"%s\" removed" build-folder)))


(defn jar
  [_]
  (clean nil)
  (b/copy-dir {:src-dirs   ["src" "resources"]
               :target-dir jar-content})

  (b/compile-clj {:basis     basis
                  :src-dirs  ["src"]
                  :class-dir jar-content})

  (b/uber {:class-dir jar-content
           :uber-file jar-file-name
           :basis     basis})

  (println (format "Jar file created: \"%s\"" jar-file-name)))
