/*
 * servo.c
 *
 *  Created on: 11 янв. 2023 г.
 *      Author: kurtlike
 */
#include "servo.h"
#define SERV_FR 50
#define SERV_FULL_MS 20
#define SERV_RIGHT_MS 0.5
#define SERV_LEFT_MS 2.5
#define SERV_MID_MS 1.5

int set_degree(struct servo_timm *servo, int8_t degree){
	if(degree > 99 || degree < -99) return -1;
	TIM_HandleTypeDef *htim = servo->htim;
	uint32_t clock = HAL_RCC_GetSysClockFreq();
	uint16_t prescaler = htim->Init.Prescaler;
	uint16_t per = clock/((prescaler+1)*SERV_FR) - 1;
	htim->Init.Period = per;
	uint16_t mid_fr = htim->Init.Period/20* SERV_MID_MS;
	uint16_t right_fr = htim->Init.Period/20* SERV_RIGHT_MS;
	uint16_t left_fr = htim->Init.Period/20* SERV_LEFT_MS;
	uint16_t new_ccr = mid_fr + (right_fr - mid_fr) * degree/99;
	switch(servo->channel){
		case TIM_CHANNEL_1:{
			htim->Instance->CCR1 = new_ccr;
			break;
		}
		case TIM_CHANNEL_2:{
			htim->Instance->CCR2 = new_ccr;
			break;
		}
		case TIM_CHANNEL_3:{
			htim->Instance->CCR3 = new_ccr;
			break;
		}
		case TIM_CHANNEL_4:{
			htim->Instance->CCR4 = new_ccr;
			break;
		}
	}
	return 0;
}
