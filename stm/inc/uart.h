/*
 * uart.h
 *
 *  Created on: Jan 10, 2023
 *      Author: kurtlike
 */

#ifndef SRC_UART_H_
#define SRC_UART_H_

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#define MAX_COMMAND_LEN 19
#define SEQ_LEN 8
#define BUFSIZE 1024


enum read_status{
	COMMAND_TYPE_READ,
	WALUE_READ,
	READ_FINISH,
};
struct command{
	int8_t ready;
	int8_t wheel;
	int8_t gas;
};
struct command command={0};
UART_HandleTypeDef huart;
unsigned char message[100] = {0};
uint8_t count = 0;
void set_uart(UART_HandleTypeDef uart){
	huart = uart;
}
int send_char(unsigned char c){
	HAL_UART_Transmit(&huart,&c,sizeof(char),10);
	return 1;
}
int send_message( char* message, size_t len){
	char* mess = message;
	while(len > 0){
		send_char(*mess);
		mess+=1;
		len--;
	}
	return 0;
}
unsigned char recieve_char(){
	unsigned char c = 0;
	if(HAL_UART_Receive(&huart,&c,sizeof(char),10) == HAL_OK){
		send_char( c);
	}
	return c;
}
int recieve_message(){
	unsigned char a = recieve_char();
	if(a != 0){
		message[count] = a;
		count++;
		if(a == '\n'|| a == '\r'){
			send_message("\r\n", 2);
			count=0;
			return 1;
		}
	}
	return 0;
}
int parse_command(){
	if(recieve_message()){
		char *parse_str = (char*)message;
		char *istr;
		istr = strtok (parse_str," ");
		command.wheel = atoi(istr);
		istr = strtok (NULL," ");
		command.gas = atoi(istr);
		command.ready = 1;
		bzero(message, 100);
		return 1;
	}
	return 0;
}
struct command receive_command(){
	if(parse_command())return command;
	struct command empty= {0, 0, 0};
	return empty;
}

#endif /* SRC_UART_H_ */
