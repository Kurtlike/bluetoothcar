/*
 * servo.h
 *
 *  Created on: 11 янв. 2023 г.
 *      Author: kurtlike
 */

#ifndef INC_SERVO_H_
#define INC_SERVO_H_
#include <stdio.h>
#include <stdlib.h>
#include <main.h>
struct servo_timm{
	TIM_HandleTypeDef *htim;
	uint8_t channel;
};
int set_degree(struct servo_timm *servo, int8_t degree);

#endif /* INC_SERVO_H_ */
